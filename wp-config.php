<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'boxout_studios');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U2:3+lWdR}b*n0X/|$0bydD4Hptdt:#ISj0i-PVX`EI:c<X_iJqW~ml&k!&n m/K');
define('SECURE_AUTH_KEY',  'HPnd0o(vdx_m](b/b.f|IPl.2lknmZ.OBUwtE%kor(qJ-6e$kI#=n)XnAlT`$>xf');
define('LOGGED_IN_KEY',    'J/}=$6<et+{IA[uD1.,PvF4!A9 MgssSkUeC!Tu&*{LE_=GMx2)FWdO(i;0M&6fX');
define('NONCE_KEY',        'kT,*&d582_ks$x@@gOS;|91jXLof%4r;]ege2{R=z:op@?$0#*c@+D9.v/M}[0AD');
define('AUTH_SALT',        'Wn7`Qqjzp_z,B*<,c2X>;T746jwpQ|e&xd<$$Rept>pP:)rwnTtqKi+1umj|DjUM');
define('SECURE_AUTH_SALT', 'eIDk>%axqP4WRdiy^b3oUx>8x}[DhHy46Haj!Fv[A-zvY,4AYMH}$D=s9J&rD/m@');
define('LOGGED_IN_SALT',   'K|-?_YIv>V==)fy69HlsH@E.C?M._?cKDiGiGTrqqD:5<RL4_A)ZDGOg0q|o_E.`');
define('NONCE_SALT',       '^{~#!_Imh~X-?bV_;|x*t7Y3~h7D4VkZ3x~On*z]{2hL_?OOXy0[@:/nI8)y?iTi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bxt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
