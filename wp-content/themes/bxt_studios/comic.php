<!-- /* Template Name: Comic Page */ -->
<?php get_header();
   $comic_issue = get_field('comic_issue'); 
   $comic_tag = get_field('comic_tag');
   $comic_synopsis = get_field('comic_synopsis');
?>

        <section class="pt32 pb48" type="comic">
            <div class="container">
                <div class="row mb24">
                    <div class="col-md-8">
                        <h2 class="headline__subtitle pt48"><?=$comic_tag ; ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <h1 class="p-text__title"><?=$comic_tag ; ?></h1>
                    </div>
                    <div class="col-md-10">
                        <div class="summary-content">
                            <p><?=$comic_synopsis ; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include('modules/characters-block.php'); ?>

        <section class="pb64" type="comic-list">
            <div class="container">
                <div class="dotted-line"></div>
                <div class="header">
                    <h2 class="title">Issues</h2>
                </div>
                <?php  if(have_rows('issues')): ?>
                <div class="featured-comics">
                    <?php   
                        while (have_rows('issues')): the_row(); 
                        $issue_image = get_sub_field('issue_image');
                        $issue_number = get_sub_field('issue_number');                                 
                        $issue_title = get_sub_field('issue_title');                                 
                    ?>
                    <a href="#" class="featured-comic">
                        <img src="<?php echo $issue_image; ?>" class="featured-comic__image" alt="" />
                        <div class="featured-comic__description">
                            <div class="featured-comic__description__bg"></div>
                            <div class="heading-set">
                                <h1 class="heading-set__primary"><?php echo $issue_number; ?></h1>
                                <p class="heading-set__secondary"><?php echo $issue_title; ?></p>
                            </div>
                        </div>
                    </a>
                <?php endwhile; ?>
                </div>
            <?php endif; ?>
            </div>
        </section>
<?php get_footer(); ?>