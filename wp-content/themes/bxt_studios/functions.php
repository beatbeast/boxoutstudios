<?php

	// require get_template_directory() . '/inc/custom_nav.php';

      function bxt_enqueue_styles() {
        wp_enqueue_style('general', get_template_directory_uri().'/css/all.css' );
        wp_enqueue_style('fonts', get_template_directory_uri().'/fonts/style.css' );

      }
      add_action( 'wp_enqueue_scripts', 'bxt_enqueue_styles' );

	// function theme_enqueue_styles() {
	// 	wp_enqueue_style('aos', get_template_directory_uri().'/css/aos.css' );
	//     wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css' );
	//     wp_enqueue_style('camera', get_template_directory_uri().'/css/camera.css' );
	//     wp_enqueue_style('color_switcher', get_template_directory_uri().'/jcss/color_switcher.css' );
	//     wp_enqueue_style('flexslider', get_template_directory_uri().'/css/flexslider.css' );
	//     wp_enqueue_style('font-awesome', get_template_directory_uri().'/css/font-awesome.min.css' );
	//     wp_enqueue_style('iconsmind', get_template_directory_uri().' /css/iconsmind.css' );
	//     wp_enqueue_style('lightbox', get_template_directory_uri().'/css/lightbox.min.css' );
	//     wp_enqueue_style('menuzord-animations', get_template_directory_uri().'/css/menuzord-animations.css' );
	//     wp_enqueue_style('menuzord', get_template_directory_uri().'/css/menuzord.css' );
	//     wp_enqueue_style('nice-select', get_template_directory_uri().'/css/nice-select.css' );
	//     wp_enqueue_style('owl.carousel', get_template_directory_uri().'/css/owl.carousel.min.css' );
	//     wp_enqueue_style('rtlskin', get_template_directory_uri().'/css/rtlskin.css' );
	//     wp_enqueue_style('skin-fonts', get_template_directory_uri().'/css/skin-fonts.css' );
	//     wp_enqueue_style('skin-fonts2', get_template_directory_uri().'/css/skin-fonts2.css' );
	//     wp_enqueue_style('spacing', get_template_directory_uri().'/css/spacing.css' );
	//     wp_enqueue_style('style', get_template_directory_uri().'/style.css' );
	//     wp_enqueue_style('responsive', get_template_directory_uri().'/css/responsive.css' );
	//     wp_enqueue_style('custom', get_template_directory_uri().'/css/custom.css' );
	// } 

	// add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

	// function theme_enqueue_scripts() {

	// 	wp_enqueue_script('vendor', get_template_directory_uri().'/js/vendor/jquery-1.12.1.min.js' );
	//     wp_enqueue_script('venobox', get_template_directory_uri().'/js/venobox/venobox.min.js' );
	//     wp_enqueue_script('aos', get_template_directory_uri().'/js/aos.js' );
	//     wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js' );
	//     wp_enqueue_script('camera', get_template_directory_uri().'/js/camera.js' );
	//     wp_enqueue_script('canvas', get_template_directory_uri().'/js/canvas.js ' );
	//     wp_enqueue_script('countdown', get_template_directory_uri().'/js/countdown.min.js' );
	//     wp_enqueue_script('flexslider', get_template_directory_uri().'/js/flexslider.min.js' );
	//     wp_enqueue_script('jquery.counterup', get_template_directory_uri().'/js/jquery.counterup.js' );
	//     wp_enqueue_script('jquery.easing', get_template_directory_uri().'/js/jquery.easing.min.js' );
	//     wp_enqueue_script('jquery.nice-select', get_template_directory_uri().'/js/jquery.nice-select.min.js' );
	//     wp_enqueue_script('lightbox', get_template_directory_uri().'/js/lightbox.min.js' );
	//     wp_enqueue_script('menuzord', get_template_directory_uri().'/js/menuzord.js' );
	//     wp_enqueue_script('main', get_template_directory_uri().'/js/main.js' );
	//     wp_enqueue_script('owl.carousel', get_template_directory_uri().'/js/owl.carousel.min.js' );
	//     wp_enqueue_script('waypoints', get_template_directory_uri().'/js/waypoints.min.js' );


	// }

	// add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


 // ================ Menu Walker Class ===================
      require get_template_directory() . '/modules/main_nav_walker.php';

/* ======= Menus ========*/
      // Activate Menu Support
      if (function_exists('add_theme_support')) {
         add_theme_support('menus');
      }

      // This adds more than one menu location
      function bxt_menus() {
         register_nav_menus(
         array(
            'footer-menu' => __( 'Footer Menu' ),
            'main-menu' => __( 'Main Menu' )
            )
         );
      }
      add_action( 'init', 'bxt_menus' );
      /* ======= Menus ========*/



?>