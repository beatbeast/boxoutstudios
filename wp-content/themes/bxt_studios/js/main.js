(function ($) {
    "use strict";
    var neighbour = $(".single_service_body").height();

    $(".set_background").each(function () {
        var thesrc = $(this).attr('src');
        $(this).parent().css("background", "url(" + thesrc + ")");
        $(this).parent().css("background-position", "center");
        $(this).parent().css("background-size", "cover");
        $(this).parent().css("background-repeat", "no-repeat");
        $(this).parent().css("height", neighbour);
        $(this).hide();

    });

    // Convert All Image to SVG
    $('img.svg').each(function () {
        var $img = $(this),
            imgID = $img.attr('id'),
            imgClass = $img.attr('class'),
            imgURL = $img.attr('src');

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass);
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);
        }, 'xml');

    });
    var wWidth = $(window).width();
    var wHight = $(window).height(),
        mSlider2 = $('#banner_slider'),
        mSlider3 = $('#banner_slider3'),
        bannerSlider = $('#banner_slider2');



    if (wWidth >= 768) {
        if (mSlider2.length) {
            mSlider2.camera({
                height: 700 + 'px',
                loader: false,
                navigation: true,
                autoPlay: true,
                fx: 'scrollBottom',
                loaderOpacity: 0,
                time: 4000,
                overlayer: true,
                playPause: false,
                pagination: false,
                thumbnails: false
            });
        }
        if (bannerSlider.length) {
            bannerSlider.camera({
                height: 520 + 'px',
                loader: false,
                navigation: true,
                autoPlay: false,
                fx: 'scrollLeft',
                time: 4000,
                overlayer: true,
                playPause: false,
                pagination: false,
                thumbnails: false,
                onEndTransition: function () {
                    $('.cameraSlide img').addClass('grow');
                }
            });
        }
        if (mSlider3.length) {
            mSlider3.camera({
                height: 750 + 'px',
                loader: false,
                navigation: true,
                autoPlay: false,
                fx: 'curtainBottomLeft',
                time: 4000,
                overlayer: true,
                playPause: false,
                pagination: false,
                thumbnails: false,
                onEndTransition: function () {
                    $('.cameraSlide img').addClass('grow');
                }
            });
        }
    }
    if (wWidth < 768) {
        if (mSlider2.length) {
            mSlider2.camera({
                height: 500 + 'px',
                loader: false,
                navigation: true,
                autoPlay: true,
                fx: 'scrollBottom',
                time: 4000,
                overlayer: true,
                playPause: false,
                pagination: false,
                thumbnails: false
            });
        }
        if (bannerSlider.length) {
            bannerSlider.camera({
                height: 500 + 'px',
                loader: false,
                navigation: true,
                autoPlay: false,
                fx: 'scrollLeft',
                time: 4000,
                overlayer: true,
                playPause: false,
                pagination: false,
                thumbnails: false,
                onEndTransition: function () {
                    $('.cameraSlide img').addClass('grow');
                }
            });
        }
        if (mSlider3.length) {
            mSlider3.camera({
                height: 500 + 'px',
                loader: false,
                navigation: true,
                autoPlay: false,
                fx: 'simpleFade',
                time: 4000,
                overlayer: true,
                playPause: false,
                pagination: false,
                thumbnails: false,
                onEndTransition: function () {
                    $('.cameraSlide img').addClass('grow');
                }
            });
        }
    }
    jQuery("#menuzord3").menuzord({
        align: "right",
        showDelay: 200,
        effect: "fade",
        animation: "fair_fade",
        indicatorFirstLevel: "<i class='fa fa-angle-down'></i>",
        indicatorSecondLevel: "<i class='fa fa-angle-right'></i>"
    });
    jQuery("#menuzord2").menuzord({
        align: "right",
        showDelay: 200,
        align: "left",
        effect: "fade",
        animation: "fair_fade",
        indicatorFirstLevel: "<i class='fa fa-angle-down'></i>",
        indicatorSecondLevel: "<i class='fa fa-angle-right'></i>"
    });
    jQuery(".menuzord3").menuzord({
        align: "right",
        showDelay: 200,
        effect: "fade",
        animation: "fair_fade",
        indicatorFirstLevel: "<i class='fa fa-angle-down'></i>",
        indicatorSecondLevel: "<i class='fa fa-angle-right'></i>"
    });
    $(document).ready(function () {
        $('.venobox').venobox();
    });


    //Food Menu Gallery Carousel
    $('.testimonial4_slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: false,
        dots: true,
        items: 1,
        autoplayHoverPause: true,
        dotsSpeed: 1500,
        autoplaySpeed: 2000,
        nav: false,
    });
    //Testimonial Slider
    $('.testimonial7_slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: false,
        dots: true,
        items: 1,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        nav: false,
    });
    //Testimonial Slider
    $('.testimonial8_slider').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: false,
        dots: true,
        items: 1,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        nav: false,
    });
    //Partners Slider
    $('.partners_carousel1').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        responsiveClass: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1500,
        dots: true,
        nav: false,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 5
            }
        }
    });
    //Carousel slider
    $('.carosuel2').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        responsiveClass: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1500,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            600: {
                items: 4
            },
            1000: {
                items: 6
            }
        }
    })
    //Carousel type3
    $('.carosuel3').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        responsiveClass: true,
        navSpeed: 1200,
        autoplayHoverPause: true,
        autoplaySpeed: 1500,
        navText: ['<i class="icon-arrow-left"></i>', '<i class="icon-arrow-right"></i>'],
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2,
                margin: 10
            },
            1000: {
                items: 3
            }
        }
    })
    //Text carousel
    $('.textContentSlider').owlCarousel({
        loop: false,
        responsiveClass: true,
        items: 1,
        navText: ['<i class="icon-arrow-left"></i>', '<i class="icon-arrow-right"></i>'],
        nav: true,
        margin: 30,
        autoplay: true,
        stagePadding: 0,
        smartSpeed: 450
    });
    //Job Post carousel
    $('.job_post_carousel').owlCarousel({
        loop: true,
        margin: 30,
        autoplay: true,
        center: true,
        dotsSpeed: 1500,
        responsiveClass: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1500,
        dots: true,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            768: {
                items: 2,
                center: false
            },
            1200: {
                items: 3
            }
        }
    });
    //Carousel5
    $('.carousel5').owlCarousel({
        loop: true,
        responsiveClass: true,
        items: 1,
        dots: true,
        nav: false,
        margin: 30,
        autoplay: true,
        stagePadding: 0,
        smartSpeed: 450
    });
    // Banner8  slider
    $('.banner8_slider').owlCarousel({
        loop: true,
        responsiveClass: true,
        items: 1,
        dots: true,
        nav: false,
        margin: 30,
        autoplay: true,
        stagePadding: 0,
        autoplaySpeed: 800,
        smartSpeed: 450
    });
    //Carousel6
    $('.carousel6').owlCarousel({
        loop: true,
        responsiveClass: true,
        items: 1,
        navText: ["<i class='icon-arrow-out-left'></i>", "<i class=' icon-arrow-out-right'></i>"],
        dots: false,
        navSpeed: 2000,
        nav: true,
        margin: 30,
        autoplay: true,
        stagePadding: 0,
        autoplaySpeed: 1500,
        smartSpeed: 450
    });

    $(".video_btn").on("click", function () {　　
        videoControl("playVideo");
        $(".bg_holder").hide();
        $('.video_container .overlay').hide();
        $(this).hide();
    });

    function videoControl(action) {
        var $playerWindow = $('.frame_video')[0].contentWindow;
        $playerWindow.postMessage('{"event":"command","func":"' + action + '","args":""}', '*');
    }

    AOS.init({
        easing: 'ease-out-back',
        duration: 1500
    });
    //Counter up
    $('.counter').counterUp({
        delay: 50,
        time: 2000
    });

    //Parallax scrolling
    if (wWidth > 767) {
        $('.calt1').parallax('90%', 0.2);
        $('.banner7').parallax('90%', 0.2);
        $('.banner8').parallax('90%', 0.2);

    }
    if (wWidth > 1200) {
        $('.quote2').parallax('90%', 0.2);

    }
    $('select').niceSelect();

    $('.switcher_btn').on('click', function () {
        $("html").attr("dir", "ltr");
        $('.wrapper').removeClass('rtl');
    });
    $('.switcher_btn2').on('click', function () {
        $("html").attr("dir", "rtl");
        $('.wrapper').addClass('rtl');
    });

    //Scroll Top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 1200) {
            $('.go-top').fadeIn();
            $('.go-top').removeClass('no-visibility');
        } else {
            $('.go-top').fadeOut();
        }
    });
    $('.go-top').on('click', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1500);
    });


    var googleMapSelector = $('#contactgoogleMap'),
        myCenter = new google.maps.LatLng(40.789886, -74.056700);

    function initialize() {
        var mapProp = {
            center: myCenter,
            zoom: 15,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#46bcec"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#c8d7d4"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#ff0000"
                        }
                    ]
                }
            ]
        };
        var map = new google.maps.Map(document.getElementById("contactgoogleMap"), mapProp);
        var marker = new google.maps.Marker({
            position: myCenter,
            animation: google.maps.Animation.BOUNCE,
            icon: 'assets/img/locator1.png'
        });
        marker.setMap(map);
    }
    if (googleMapSelector.length) {
        google.maps.event.addDomListener(window, 'load', initialize);
    }

    $('[data-toggle="tooltip"]').tooltip();


    var container = $('.grid_tab');
    var tabPen = $('.my_tab');
    $('.my_tab').on('click', function () {

        $(this).addClass('active');
        $(this).siblings('.my_tab').removeClass('active');
        $(this).parents('.grid_tab').find(tabPen).not($(this)).removeClass('active');
    });

    $('.banner9').on('mousemove', function (e) {

        var $wHeight = $(window).height(),
            $wWidth = $(window).width(),
            $wCh = $wHeight / 2,
            $wCw = $wWidth / 2,
            $yDist = (e.pageY - $wCh),
            $xDist = (e.pageX - $wCw),
            $yMove = -1 * $yDist * (2 / $wCh),
            $xMove = -1 * $xDist * (4 / $wCw);
        $('#logo_move').css({
            'transform': 'translateY(' + $yMove + '%) translateX(' + $xMove + '%)'
        });


    }).on('mouseenter', function () {
        setTimeout(function () {
            $('#logo_move').css('transition', 'none');
        }, 500);
    }).on('mouseleave', function () {
        $('#logo_move').css({
            'transition': 'all 1s linear',
            'transform': 'translateY(0) translateX(0)'
        });
    });

    var wWidth = $(window).width();
    if (wWidth >= 480) {
        function equalHeight() {
            $('.hParents').each(function () {
                var highestBox = 0;

                $('.hChild', this).each(function () {
                    if ($(this).height() > highestBox) {
                        highestBox = $(this).height();
                    }

                });
                $('.hChild', this).height(highestBox);
            });
        }

        $(document).ready(equalHeight);
        $(window).bind("resize", equalHeight);
    }
    $('#countdownTimer').countdown('2017/08/25', function (event) {
        $(this).html(event.strftime('<ul><li>%D<span>Days</span></li><li>%H<span>Hours</span></li><li>%M<span>Minutes</span></li><li>%S<span>Seconds</span></li><ul>'));
    });

    var colorTrigger = $('.color_changer ul li'),
        body = $('body'),
        colorBox = $('.color_changer');
    colorTrigger.on('click', function () {
        var CCcolor = $(this).data('color'),
            colorList = colorTrigger.map(function () {
                return $(this).data('color');
            }).get();
        colorList = colorList.join(' ');
        body.removeClass(colorList).addClass(CCcolor);
        colorTrigger.removeClass('active');
        $(this).addClass('active');

    });


    $('.offcanvas_btn').on('click', function () {
        $('.single_canvas').addClass('open');
        $('.single_canvas').removeClass('close');
        $('head').append('<style>.wrapper::before{visibility: visible;opacity:1;}</style>');
    });
    $('.canvas_cancel').on('click', function () {
        $('.single_canvas').addClass('close');
        $('.single_canvas').removeClass('open');
        $('head').append('<style>.wrapper::before{visibility: hidden;opacity:0;}</style>');
    });
    var logoCheck = $('.canvas_light');
    if (logoCheck.length) {
        document.getElementById('clogo1').src = 'assets/img/site_logo2.png';
    } else {
        document.getElementsByClassName('clogo1').src = 'assets/img/site_logo2.png';
    }

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 0) {
            $('.fst.nvaigation_area').addClass('fair_sticky');
        } else {
            $('.fst.nvaigation_area').removeClass('fair_sticky');
        }
    });
    var child1 = $('.child1').height(),
        child2 = $('.child2').height();
    if (child2 > child1) {
        $('.child1').css({
            'height': child2 + 'px'
        });
    } else {
        $('.child2').css({
            'height': child1 + 'px'
        });
    }

    $('.switcher_gearbox').on('click', function () {
        $('.switcher_box').toggleClass('open');
    });
    $('.sticky_on_btn').on('click', function () {
        //event.preventDefault();
        $('.nvaigation_area').addClass('fair_sticky');
        $(this).addClass('active');
        $(this).siblings('.sticky_off_btn').addClass('inactive');
        $(this).removeClass('inactive');
        $('.nvaigation_area').addClass('fst');
    });
    $('.sticky_off_btn').on('click', function () {
        //event.preventDefault();
        $('.nvaigation_area').removeClass('fair_sticky');
        $(this).removeClass('inactive');
        $(this).siblings('.sticky_on_btn').addClass('inactive');
        $(this).siblings('.sticky_on_btn').removeClass('active');
        $('.nvaigation_area').removeClass('fst');
    });



})(jQuery);
