<?php get_header();

    /* Get the field variable */ 

      $cover_image = get_field('cover_image');
      $cover_issue = get_field('cover_issue');
      $cover_title = get_field('cover_title');
      $cover_caption = get_field('cover_caption');
      $section_tagline = get_field('section_tagline');
      $section_title = get_field('section_title');
      $section_caption = get_field('section_caption');
      $image_1 = get_field('image_1');
      $image_2 = get_field('image_2');
      $image_3 = get_field('image_3');
      $button_title = get_field('button_title');
      $button_label = get_field('button_label');
      $button_link = get_field('button_link');


?>

    <?php include('modules/slider-block.php'); ?>
        <section class="pt64 pt-xs-80">
            <div class="container">
                <div class="hero hero--homepage">
                    <div class="hero-background">
                        <a href="#">
                            <img src="<?php echo $cover_image ; ?>" alt="">
                        </a>
                    </div>
                    <div class="hero-content">
                        <a href="#" class="hero-linkWrapper">
                            <h4 class="hero-pretitle"><?php echo $cover_issue  ; ?></h4>
                            <h3 class="hero-title"><?php echo $cover_title  ; ?></h3>
                            <p><?php echo $cover_caption  ; ?></p>
                            <!--                            <div class="hero-link" data-reactid=".y92b487bwg.1.2.0.0.0.1.0:$0.0.3"><span class="button button--small button--primary button--light button--arrow">Watch the Episode</span></div>-->
                        </a>
                    </div>
                    <div class="issueMini">
                        <div class="issueMini-heading">Past issues</div>
                        <ol class="issueMini-list">
                            <li class="issueMini-listItem">
                                <a href="#">
                                    <span class="issueMini-release">Issue #1</span>
                                    <span class="issueMini-title">Sorek Valley</span>
                                </a>
                            </li>
                            <li class="issueMini-listItem">
                                <a href="#">
                                    <span class="issueMini-release">Issue #2</span>
                                    <span class="issueMini-title">The Oracle</span>
                                </a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row">
                    <?php 
                     //the_query
                    $bxt_query = new WP_QUERY( array(
                        'cat' => '',
                        'posts_per_page' => 4,
                    ));
                    if ( $bxt_query->have_posts() ) :
                    while ( $bxt_query->have_posts() ) : $bxt_query->the_post();
                          $date = get_field('date');
                          $post_caption = get_field('post_caption');
                          $issue = get_field('issue');
                          $featured_image = get_field('featured_image');
                          $image_exist = ($featured_image == '')? get_bloginfo('template_directory').'/img/badge.png' : $image;
                           ?>
                    <div class="col-md-3">
                        <div class="component--cardContainer">
                            <a href="<?php the_permalink(); ?>" class="card--link">
                                <div class="component--imageContainer">
                                    <img src="<?php echo $featured_image; ?>">
                                </div>
                                <div class="component--textContainer">
                                    <div class="cardtext--body">
                                        <p class="cardtext--label"><?php echo $issue ; ?></p>
                                        <p class="cardtext--title"><?php the_title(); ?></p>
                                        <p class="cardtext--details"><?php echo $post_caption ; ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                      <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>


        <?php include('modules/gallery-block.php'); ?>

        <section>
            <div class="container">
                <div class="headline__teaser-wrapper">
                    <div class="row">
                    <?php 
                     //the_query
                    $query = new WP_QUERY( array(
                        'cat' => '4,5,6',
                        'posts_per_page' => 3,
                    ));
                    if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post();
                          $date = get_field('date');
                          $post_caption = get_field('post_caption');
                          $featured_image = get_field('featured_image');
                          $image_exist = ($featured_image == '')? get_bloginfo('template_directory').'/img/badge.png' : $image;
                           ?>
                        <div class="col-md-4">
                            <div class="headline-teaser headline-teaser__small">
                                <div class="headline-teaser__image-container">
                                    <a href="#">
                                        <div class="headline-teaser__image-placeholder" style="">
                                            <img alt="" src="<?php echo $featured_image; ?>">
                                        </div>
                                    </a>
                                </div>
                                <div class="headline-teaser__content">
                                    <div class="headline-teaser__meta">
                                        <div class="headline-teaser__tag"><?php the_category(); ?></div>
                                    </div>
                                    <div class="headline-teaser__heading">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </div>
                                    <p><?php echo $post_caption ; ?></p>
                                    <!--
                                    <div class="btn-group">
                                        <a href="#" class="btn"><span></span>Buy Digital</a>
                                        <a href="#" class="btn"><span></span>Subscribe</a>
                                    </div>
-->
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                      <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="pb96">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="headline-group">
                            <div class="subtitle"><?=$section_tagline ; ?></div>
                            <h2 class="h1-match title"><?=$section_title ; ?></h2>
                            <span class="rule"></span>
                        </div>
                        <p><?=$section_caption ; ?></p>
                        <a class="funky-btn" href="<?=$button_link ; ?>">
                            <div class="text"><?=$button_label ; ?></div>
                            <div class="bg"></div>
                        </a>
                    </div>
                    <div class="col-md-8">
                        <div class="image-container">
                            <img src="<?=$image_1 ; ?>" class="image-content image-1 hidden-xs hidden-sm" alt="">
                            <img src="<?=$image_2 ; ?>" class="image-content image-2 hidden-xs hidden-sm" alt="">
                            <img src="<?=$image_3 ; ?>" class="image-content image-3 hidden-xs hidden-sm" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section type="news">
            <div class="container">
                <div class="header">
                    <h2 class="title">News</h2>
                    <a class="header-link" href="#" title="">
                        <span class="header-link-arrow">
                            <i class="icon-Right"></i>
                        </span>
                        <span class="header-link-text">See all News</span>
                    </a>
                </div>
                <div class="row">
                    <?php 
                     //the_query
                    $the_query = new WP_QUERY( array(
                        'cat' => '',
                        'posts_per_page' => 3,
                        'offset' => 7,
                    ));
                    if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                          $date = get_field('date');
                          $post_caption = get_field('post_caption');
                          //$image_exist = ($image == '')? get_bloginfo('template_directory').'/img/Image-Placeholder.jpg' : $image;
                           ?>
                    <div class="col-md-4">
                        <div class="news-item">
                            <a href="<?php the_permalink(); ?>">
                                <div class="news-title">
                                    <h3>
                                        <span><?php the_title(); ?></span>
                                    </h3>
                                </div>
                                <div class="date"><?php the_date('F j, Y') ?></div>
                                <p class="summary"><?=$post_caption ; ?></p>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                      <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>

<?php get_footer(); ?>