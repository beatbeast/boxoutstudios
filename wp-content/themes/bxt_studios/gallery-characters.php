<!-- /* Template Name: Characters Gallery Page */ -->
<?php get_header(); 
         /* Get the field variable */ 
      $page_title = get_field('character_page_title');
?>

        <section class="pt120 pb96">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="headline-group pt48">
                            <h2 class="h1-match title"><?php echo $page_title ; ?></h2>
                            <span class="rule"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="mason-gal">
                        <?php
                            $images = get_field('character_image');
                            if( $images ): 
                            foreach( $images as $image ): ?>
                            <a href="<?php echo $image['url']; ?>" class="image-link" data-lightbox="Gallery 1">
                                <img src="<?php echo $image['url']; ?>" alt="" />
                            </a>
                        <?php endforeach; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php get_footer(); ?>