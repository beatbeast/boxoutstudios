<?php get_header(); 
    

     /* Get the field variable */ 
  $what_category_does_this_post_belong_to = get_field('what_category_does_this_post_belong_to');
  $date = get_field('date');
  $post_title = get_field('section_title');
  $featured_image = get_field('featured_image');
  $post_caption = get_field('post_caption');
  $first_parting = get_field('first_parting');
  $quote = get_field('quote');
  $second_parting = get_field('second_parting');

?>


        <section class="pt120 pb32" type="news">
            <div class="container">
                <div class="row mt64 mb32">
                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                        <div class="news-item">
                            <div class="date"><?//php the_category(); ?> | <?php the_date('F j, Y'); ?></div>
                            <div class="news-title">
                                <h1>
                                    <span><?php the_title(); ?></span>
                                </h1>
                            </div>
                            <div class="img-wrap">
                                <img src="<?=$featured_image; ?>" alt="" />
                            </div>
                            <div class="news-body-container">
                                <ul class="social-vertical">
                                    <li class="social-item">
                                        <a class="social-link" href="#" title="">
                                            <span class="svg svg-facebook">
                                            <svg width="35" height="35" viewBox="0 0 178 178" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                <g transform="matrix(3.34731,0,0,3.34731,-196.499,-220.62)">
                                                    <path d="M88.937,82.289L88.937,78.231C88.937,76.284 90.225,75.818 91.158,75.818C92.063,75.818 96.834,75.818 96.834,75.818L96.834,67.154L89.019,67.154C80.328,67.154 78.381,73.569 78.381,77.737L78.381,82.289L73.335,82.289L73.335,92.406L78.435,92.406C78.435,103.894 78.435,117.686 78.435,117.686L88.553,117.686C88.553,117.686 88.553,103.702 88.553,92.406L96.038,92.406L96.368,88.431L96.971,82.289L88.937,82.289Z"></path>
                                                </g>
                                            </svg>
                                          </span>
                                            <span class="social-text">Share</span>
                                        </a>
                                    </li>
                                    <li class="social-item">
                                        <a class="social-link" href="#" title="">
                                            <span class="svg svg-twitter">
                                            <svg width="35" height="35" viewBox="0 0 178 178" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                <g transform="matrix(3.32062,0,0,3.32062,-2284.49,-627.366)">
                                                    <path d="M740.126,200.023C738.288,200.819 736.288,201.367 734.177,201.641C736.342,200.353 737.986,198.378 738.756,195.965C736.754,197.144 734.506,197.994 732.148,198.46C730.281,196.459 727.569,195.225 724.553,195.225C718.85,195.225 714.188,199.804 714.188,205.424C714.188,206.247 714.326,207.015 714.49,207.756C705.854,207.344 698.203,203.286 693.103,197.089C692.226,198.625 691.678,200.353 691.678,202.217C691.678,205.781 693.542,208.907 696.283,210.717C694.584,210.689 692.993,210.223 691.596,209.455C691.596,209.51 691.596,209.538 691.596,209.592C691.596,210.908 691.843,212.115 692.308,213.266C693.57,216.475 696.449,218.887 699.932,219.573C699.025,219.82 698.148,219.956 697.189,219.956C696.531,219.956 695.873,219.874 695.241,219.737C696.586,223.795 700.396,226.756 704.922,226.866C701.356,229.608 696.915,231.226 692.062,231.226C691.211,231.226 690.417,231.17 689.567,231.088C694.173,233.968 699.629,235.667 705.47,235.667C721.674,235.667 731.682,224.453 734.285,212.8C734.753,210.717 734.971,208.66 734.971,206.631C734.971,206.192 734.971,205.727 734.971,205.315C736.973,203.862 738.756,202.052 740.126,200.023Z"></path>
                                                </g>
                                            </svg>
                                          </span>
                                            <span class="social-text">Tweet</span>
                                        </a>
                                    </li>
                                    <li class="social-item">
                                        <a class="social-link" href="#" title="">
                                            <span class="svg svg-linkedin">
                                            <svg width="35" height="35" viewBox="0 0 178 178" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                <g transform="matrix(3.64545,0,0,3.64545,-2516.74,-1144.74)">
                                                    <path d="M697.71,316.14C694.831,316.14 692.5,318.471 692.5,321.35C692.5,324.229 694.831,326.56 697.71,326.56C700.589,326.56 702.919,324.229 702.919,321.35C702.919,318.471 700.589,316.14 697.71,316.14ZM736.946,340.187C736.26,334.538 733.684,331.029 726.061,331.029C721.591,331.029 718.575,332.674 717.314,335.06L717.177,335.06L717.177,331.029L708.868,331.029L708.868,360.833L717.561,360.833L717.561,346.055C717.561,342.16 718.328,338.405 723.101,338.405C727.897,338.405 728.253,342.846 728.253,346.328L728.253,360.833L737.192,360.833L737.192,344.464C737.192,342.928 737.109,341.503 736.946,340.187ZM692.5,338.953L692.5,360.833L702.919,360.833L702.919,337.17L702.919,331.029L692.5,331.029L692.5,338.953Z"></path>
                                                </g>
                                            </svg>
                                          </span>
                                            <span class="social-text">Share</span>
                                        </a>
                                    </li>
                                    <li class="social-item">
                                        <a class="social-link" onclick="window.print()" href="#" title="">
                                            <span class="svg svg-linkedin">
                                           <svg width="35" height="35" viewBox="0 0 178 178" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                <g transform="matrix(2.93656,0,0,2.93656,-44.2513,-43.0377)">
                                                    <path d="M17.706,65.937L17.706,45.515L72.405,45.517L72.405,59.419C72.405,63.013 69.481,65.937 65.887,65.937L63.33,65.937L63.33,70.271L26.781,70.271L26.781,65.937L17.706,65.937ZM59.93,66.872L59.93,59.933L30.181,59.933L30.181,66.872L59.93,66.872ZM49.162,55.397L49.162,51.997L46.045,51.997L46.045,55.397L49.162,55.397ZM44.062,55.397L44.062,51.997L40.946,51.997L40.946,55.397L44.062,55.397ZM63.33,21.292L63.33,42.599L26.78,42.599L26.78,21.292L63.33,21.292Z"></path>
                                                </g>
                                            </svg>
                                          </span>
                                            <span class="social-text">Print</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="news-body">
                                    <p class="news-summary"<?=$post_caption; ?></p>
                                    <p><?=$first_parting; ?></p>
                                    <blockquote>"<?=$quote; ?>"</blockquote>
                                    <p><?=$second_parting; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="pt96 pb24" type="news">
            <div class="curved-line"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="headline-group">
                            <h2 class="h1-match title">Related News</h2>
                            <span class="rule"></span>
                        </div>
                    </div>
                </div>
                <div class="row mb32">
                     <?php 
                     //the_query
                    $the_query = new WP_QUERY( array(
                        'cat' => '',
                        'posts_per_page' => 3,
                        'offset' => 7,
                    ));
                    if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                          $date = get_field('date');
                          $post_caption = get_field('post_caption');
                          $featured_image = get_field('featured_image');
                          //$image_exist = ($image == '')? get_bloginfo('template_directory').'/img/Image-Placeholder.jpg' : $image;
                           ?>
                    <div class="col-md-4">
                        <div class="news-item mb24">
                            <a href="<?php the_permalink(); ?>">
                                <div class="img-wrap">
                                    <img src="<?php echo $featured_image; ?>" alt="" />
                                </div>
                                <div class="news-title">
                                    <h3>
                                        <span><?php the_title(); ?></span>
                                    </h3>
                                </div>
                                <div class="date"><?php the_date('F j, Y') ?></div>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                      <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>


        <?php echo do_shortcode( '[mc4wp_form id="321"]' ); ?>
       <!--  <section type="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
                        <h2 class="newsletter-title">Stay Updated</h2>
                        <p class="newsletter-text mb16">Subscribe now for the latest on Bxt Studios creator-owned comics and related merchandise.</p>
                        <form method="GET" action="#" class="newsletter-form">
                            <input type="email" name="EMAIL" id="email" placeholder="Email Address">
                            <button type="submit">
                        <span class="svg" style="width:25px; height:25px;">
                            <svg width="25" height="25" viewBox="0 0 30 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                <g transform="matrix(1,0,0,1,-36.1133,-46.9252)">
                                    <g transform="matrix(0.44544,0,0,0.44544,28.5408,-393.331)">
                                        <path d="M83,1002.36L59,988.362L59,1000.36L17,1000.36L17,1004.36L59,1004.36L59,1016.36L83,1002.36Z" style="fill-rule:nonzero;"></path>
                                    </g>
                                </g>
                            </svg>
                          </span>
                    </button>
                        </form>
                    </div>
                </div>
            </div>
        </section> -->
<?php get_footer(); ?>