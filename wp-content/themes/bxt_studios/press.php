<!-- /* Template Name: Characters Press Page */ -->
<?php get_header(); ?>


        <section class="pt64 pb96" type="news">
            <div class="container">
                <div class="row mb32">
                 <?php 
                     //the_query
                    $the_query = new WP_QUERY( array(
                        'cat' => '',
                        'posts_per_page' => 9,
                    ));
                    if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                          $date = get_field('date');
                          $post_caption = get_field('post_caption');
                          $featured_image = get_field('featured_image');
                          //$image_exist = ($featured_image == '')? get_bloginfo('template_directory').'/img/Image-Placeholder.jpg' : $image;
                ?>
                    <div class="col-md-4">
                        <div class="news-item">
                            <a href="<?php the_permalink(); ?>">
                                <div class="img-wrap">
                                    <img src="<?php echo $featured_image; ?>" alt="" />
                                </div>
                                <div class="news-title">
                                    <h3>
                                        <span><?php the_title(); ?></span>
                                    </h3>
                                </div>
                                <div class="date"><?php the_date('F j, Y') ?></div>
                                <p class="summary"><?=$post_caption ; ?></p>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>

       <!--  <section type="newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
                        <h2 class="newsletter-title">Stay Updated</h2>
                        <p class="newsletter-text mb16">Subscribe now for the latest on Bxt Studios creator-owned comics and related merchandise.</p>
                        <form method="GET" action="#" class="newsletter-form">
                            <input type="email" name="EMAIL" id="email" placeholder="Email Address">
                            <button type="submit">
                        <span class="svg" style="width:25px; height:25px;">
                            <svg width="25" height="25" viewBox="0 0 30 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                <g transform="matrix(1,0,0,1,-36.1133,-46.9252)">
                                    <g transform="matrix(0.44544,0,0,0.44544,28.5408,-393.331)">
                                        <path d="M83,1002.36L59,988.362L59,1000.36L17,1000.36L17,1004.36L59,1004.36L59,1016.36L83,1002.36Z" style="fill-rule:nonzero;"></path>
                                    </g>
                                </g>
                            </svg>
                          </span>
                    </button>
                        </form>
                    </div>
                </div>
            </div>
        </section> -->
        <?php echo do_shortcode( '[mc4wp_form id="321"]' ); ?>

<?php get_footer();?>
