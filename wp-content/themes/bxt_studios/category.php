<?php get_header();

        /* Get the field variable */ ;
  // $post_caption = get_field('post_caption');

  // Category Name and ID
  $cat_news = 3;
  $cat_merchandise = 4;
  $cat_blacksage_the_rising = 6;
  $cat_press = 2;
  $cat_the_shepherd = 5;

?>


        <section class="pt64 pb96" type="news">
            <div class="container">
                <div class="row mb32">
                 <?php 
                    if ( have_posts() ) : 
                    while ( have_posts() ) : the_post();
                    $featured_image = get_field('featured_image');
                    $post_caption = get_field('post_caption');   
                    // $ = get_field('');   
                    // $ = get_field('');   
                    // $ = get_field('');   
                    // $ = get_field('');   
                     // $image_exist = ($image == '')? get_bloginfo('template_directory').'/img/image-placeholder-for-website.jpg' : $image; ?>
                    <div class="col-md-4">
                        <div class="news-item">
                            <a href="<?php the_permalink(); ?>">
                                <div class="img-wrap">
                                    <img src="<?=$featured_image ; ?>" alt="" />
                                </div>
                                <div class="news-title">
                                    <h3>
                                        <span><?php the_title(); ?></span>
                                    </h3>
                                </div>
                                <div class="date"><?php the_date('F j, Y') ?></div>
                                <p class="summary"><?=$post_caption ; ?></p>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>


        <?php echo do_shortcode( '[mc4wp_form id="321"]' ); ?>
<?php get_footer();?>
