<!DOCTYPE html>
<html lang="en" dir="ltr">

<?php include('modules/header-info.php'); ?>

<body class="dark">
    <div class="site-noise"></div>
    <div class="wrapper">
        <?php include('modules/main-nav.php');  
          // if ( !is_404()) {
          //   include('modules/main-nav.php');
          // } 
            if (is_page_template('comic.php')) {
              include 'modules/comic_banner.php';
            }
          	if (is_front_page()):
          		include 'modules/home_banner_slider.php';
          	else:
          		if (is_page() && !is_page_template('comic.php') && !is_page_template('contact.php') && !is_page_template('about.php')):
          			include 'modules/page_banner.php';
          		elseif (is_category()):
          			include 'modules/category_banner.php';
          		endif;		
          	endif		
      	?> 