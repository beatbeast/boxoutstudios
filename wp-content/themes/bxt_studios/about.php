<!-- /* Template Name: About BoxOut Studios */ -->
<?php get_header(); 
     /* Get the field variable */ 
  $page_title = get_field('page_title');
  $page_tagline = get_field('page_tagline');
  $page_caption = get_field('page_caption');
  $featured_image = get_field('featured_image');
?>
        <?php include('modules/founder-modal.php'); ?>
        <section class="pt120 pb32">
            <div class="container">
                <div class="row mb24">
                    <div class="col-md-8">
                        <h2 class="headline__subtitle pt48"><?=$page_title ;?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h1 class="p-text__title"><?=$page_tagline ;?></h1>
                    </div>
                    <div class="col-md-3">
                        <img src="<?=$featured_image ;?>" class="mb16" alt="">
                    </div>
                    <div class="col-md-6"><?=$page_caption ;?></p>
                    </div>
                </div>

                <!--
                <div class="row">
                    <div class="col-md-12">
                        <div class="about-gallery">
                            <img src="assets/img/about-pic-1.jpg" class="about-image image-1">
                            <img src="assets/img/about-pic-2.jpg" class="about-image image-2">
                        </div>
                    </div>
                </div>
-->
            </div>
        </section>

        <section class="pt24" type="founders">
            <div class="curved-line"></div>
            <div class="container">
                <div class="row">
                <?php  if(have_rows('creator_profile')): ?>
                    <div class="col-md-12">
<!--                        <h1 class="p-text__title pb40 pt48">Meet the Founders.</h1>-->
                        <div class="headline-group pt48">
                            <h2 class="h1-match title">Meet<span class="break"></span>the Creators</h2>
                            <span class="rule"></span>
                        </div>
                        <div class="row">
                        <?php   
                            while (have_rows('creator_profile')): the_row();
                            $creator_name = get_sub_field('creator_name');                            
                            $creator_image = get_sub_field('creator_image');                            
                            $creator_position = get_sub_field('creator_position');                            
                            $creator_details_caption_ = get_sub_field('creator_details_caption_');                            
                            $creator_details = get_sub_field('creator_details');                          
                        ?>
                            <div class="col-md-3 elements__item">
                                <img src="<?=$creator_image ;?>" alt="" />
                                <div class="elements__action">
                                    <div class="elements__hover">
                                        <a href="#" class="elements__about elements__anchor" data-toggle="modal" data-target="#founderModal1">
                                            <div class="ui-plus">
                                                <div class="ui-plus__inner">
                                                    <svg width="100%" height="100%" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;">
                                                <g transform="matrix(1,0,0,1,-20.5888,-20.3918)">
                                                    <path d="M47,23L47,47L23,47L23,53L47,53L47,77L53,77L53,53L77,53L77,47L53,47L53,23L47,23Z" style="fill-rule:nonzero;"></path>
                                                </g>
                                            </svg>
                                                </div>
                                            </div>
                                            <span class="elements__more" style="transform: matrix(1, 0, 0, 1, 0, 0);"><?=$creator_name ;?></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </section>
<?php get_footer(); ?>