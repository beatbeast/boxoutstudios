<!-- /* Template Name: Contact Page */ -->
<?php get_header(); 
    $contact_area_caption = get_field('contact_area_caption');
    $contact_tag = get_field('contact_tag');
?>

        <section class="pt120 pb96">
            <div class="container">
                <div class="row mb24">
                    <div class="col-md-8">
                        <h2 class="headline__subtitle pt48"><?=$contact_area_caption ; ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <h1 class="p-text__title"><?=$contact_tag ; ?></h1>
                    </div>
                    <?php  if(have_rows('contact_emails')):   
                            while (have_rows('contact_emails')): the_row();
                            $tag = get_sub_field('tag');
                            $email = get_sub_field('email');
                    ?>
                    <div class="col-md-3">
                        <p class="contact-page__department"><?=$tag ; ?></p>
                        <a class="contact-page__email" href="mailto:<?=$email ; ?>"><?=$email ; ?></a>
                    </div>
                <?php endwhile; endif; ?>
                </div>
            </div>
        </section>

        <section type="testimonial">
            <div class="curved-line"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-12">
                        <div class="what-people-say align_center">
                            <span>❤❤❤</span>
                            <h2>What people say about us</h2>
                        </div>
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#quote-carousel" data-slide-to="1"></li>
                                <li data-target="#quote-carousel" data-slide-to="2"></li>
                            </ol>
                            <?php  if(have_rows('testimonials')): 
                                $counter = 0;
                                $class_active = '';
                            ?>
                            <div class="carousel-inner">
                                <?php   
                                    while (have_rows('testimonials')): the_row();
                                    $feedback = get_sub_field('feedback');              
                                    $name = get_sub_field('name');              
                                    $designation = get_sub_field('designation');
                                    if ($counter == 0) {
                                    $class_active = 'active';
                                    } else {
                                    $class_active = '';
                                    }              
                                ?>
                                <div class="item <?php echo $class_active ; ?>">
                                    <p><?=$feedback ; ?></p>
                                    <small><strong><?=$name ; ?></strong><?=$designation ; ?></small>
                                </div>
                            <?php $counter++; endwhile; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); ?>
