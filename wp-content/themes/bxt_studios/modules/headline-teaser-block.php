<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="headline-teaser headline-teaser__type2">
                    <div class="headline-teaser__content">
                        <div class="headline-teaser__meta">
                            <a href="#" class="headline-teaser__tag">World</a>
                        </div>
                        <div class="headline-teaser__heading">
                            <a href="#">Cambodia opposition leader arrested for alleged plot</a>
                        </div>
                        <p class="headline-teaser__body">Kem Sokha accused of treason amid escalating crackdown on critics of leader Hun Sen</p>
                    </div>
                    <div class="headline-teaser__image-container">
                        <a href="#">
                            <div class="headline-teaser__image-placeholder">
                                <img src="<?php bloginfo('template_directory'); ?>/img/headline-teaser3.jpg" alt="">
                            </div>
                        </a>
                    </div>
                </div>

                <div class="headline-teaser headline-teaser__type2">
                    <div class="headline-teaser__content">
                        <div class="headline-teaser__meta">
                            <a href="#" class="headline-teaser__tag">Economy</a>
                        </div>
                        <div class="headline-teaser__heading">
                            <a href="#">Texas floods push US petrol prices to 2-year high</a>
                        </div>
                        <p class="headline-teaser__body">Kem Sokha accused of treason amid escalating crackdown on critics of leader Hun Sen</p>
                    </div>
                    <div class="headline-teaser__image-container">
                        <a href="#">
                            <div class="headline-teaser__image-placeholder">
                                <img src="<?php bloginfo('template_directory'); ?>/img/headline-teaser2.jpg" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="headline-teaser headline-teaser__type2">
                    <div class="headline-teaser__content">
                        <div class="headline-teaser__meta">
                            <a href="#" class="headline-teaser__tag">Business</a>
                        </div>
                        <div class="headline-teaser__heading">
                            <a href="#">Kenyan Stock Exchange Halts Trading After Supreme Court Ruling</a>
                        </div>
                        <p class="headline-teaser__body">Kem Sokha accused of treason amid escalating crackdown on critics of leader Hun Sen</p>
                    </div>
                    <div class="headline-teaser__image-container">
                        <a href="#">
                            <div class="headline-teaser__image-placeholder">
                                <img src="<?php bloginfo('template_directory'); ?>/img/headline-teaser4.jpg" alt="">
                            </div>
                        </a>
                    </div>
                </div>

                <div class="headline-teaser headline-teaser__type2">
                    <div class="headline-teaser__content">
                        <div class="headline-teaser__meta">
                            <a href="#" class="headline-teaser__tag">Politics</a>
                        </div>
                        <div class="headline-teaser__heading">
                            <a href="#">Six Countries To Participate In BRICS Business Forum</a>
                        </div>
                        <p class="headline-teaser__body">Kem Sokha accused of treason amid escalating crackdown on critics of leader Hun Sen</p>
                    </div>
                    <div class="headline-teaser__image-container">
                        <a href="#">
                            <div class="headline-teaser__image-placeholder">
                                <img src="<?php bloginfo('template_directory'); ?>/img/headline-teaser5.jpg" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
