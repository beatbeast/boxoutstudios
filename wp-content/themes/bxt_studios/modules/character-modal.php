 <?php
    if(have_rows('characters')):   
    while (have_rows('characters')): the_row(); 
    $character_cover_image = get_sub_field('character_cover_image');
    $character_image = get_sub_field('character_image');                                 
    $character_designation = get_sub_field('character_designation');                                 
    $character_name = get_sub_field('character_name');                                 
    $character_caption = get_sub_field('character_caption');                                 
    $character_details = get_sub_field('character_details');                                 
?>
<div class="modal fade" id="characterModal1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo $character_image ; ?>" alt="" />
                    </div>
                    <div class="col-md-8">
                        <div class="card__title">
                            <span class="employee-name"><?php echo $character_name ; ?></span>
                            <span class="employee-function"><?php echo $character_designation ; ?></span>
                        </div>
                        <hr>
                        <p class="caption-text"><?php echo $character_caption ; ?></p>
                        <p><?php echo $character_details ; ?></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php endwhile; endif; ; ?>