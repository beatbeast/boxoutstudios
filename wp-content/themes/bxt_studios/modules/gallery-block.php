<section type="gallery" class="pb40">
    <div class="container">
        <div class="dotted-line"></div>
        <div class="header">
            <h2 class="title">Gallery</h2>
            <a class="header-link" href="#" title="">
                <span class="header-link-arrow">
                            <i class="icon-Right"></i>
                        </span>
                <span class="header-link-text">Visit our Gallery</span>
            </a>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="image-slider slider-all-controls controls-inside">
                    <ul class="slides">
                         <?php 
                            $gallery = get_field('gallery');
                        ?>
                        <?php foreach( $gallery as $image ): ?>
                        <li>
                            <img alt="" src="<?php echo $image['url']; ?>" />
                        </li>
                        <?php endforeach; ?><!-- 
                        <li>
                            <img alt="" src="<?php //bloginfo('template_directory'); ?>/img/slider-img1.jpg" />
                        </li>
                        <li>
                            <img alt="" src="<?php //bloginfo('template_directory'); ?>/img/slider-img2.jpg" />
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
