<section class="pt64">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="p-text__title pb64">Meet the Founders.</h1>
                <div class="m-people people-odd">
                    <div class="people-box">
                        <div class="people-social">
                            <ul>
                                <li><a target="_blank" href="https://www.facebook.com/#"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank" href="https://twitter.com/s_o_l_z"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="https://www.instagram.com/s_o_l_z"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <div class="people-image-wrapper">
                            <div class="people-image">
                                <img src="<?php bloginfo('template_directory'); ?>/img/people-img-1.jpg" alt="">
                            </div>
                        </div>
                        <div class="people-info">
                            <h2 class="people-name">Sola Adebayo</h2>
                            <div class="separator"></div>
                            <h5 class="people-role">Co-Founder</h5>
                            <p class="people-bio">Ascendent Berlin-based producer Palms Trax (a.k.a. Jay Donaldson) returns to Dekmantel in early 2016 with a new EP, High Point on Low Ground. Donaldson debuted on Lobster Theremin back in 2013, helping to get the young London imprint off the ground. His upcoming three-tracker follows this year's In Gold EP for the Dutch tastemakers, which swiftly sold out and will see a repress in January thanks to opportunistic Discogs prices.</p>
                        </div>
                    </div>
                </div>

                <div class="m-people people-even">
                    <div class="people-box">
                        <div class="people-social">
                            <ul>
                                <li><a target="_blank" href="https://www.facebook.com/#"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank" href="https://twitter.com/s_o_l_z"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" href="https://www.instagram.com/s_o_l_z"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <div class="people-image-wrapper">
                            <div class="people-image">
                                <img src="<?php bloginfo('template_directory'); ?>/img/people-img-2.jpg" alt="">
                            </div>
                        </div>
                        <div class="people-info">
                            <h2 class="people-name">Bill Bidiaque</h2>
                            <div class="separator"></div>
                            <h5 class="people-role">Co-Founder</h5>
                            <p class="people-bio">After spending his youth in the same small town near Berlin as techno greats Marcel Detmann and Marcel Fengler, Answer Code Request discovered the art of DJing and producing. His first EP combined Berlin’s techno heritage with Detroit melancholia, the heyday of the UK’s ‘artificial Intelligence’, and the rhythmic innovations of the most current bass-music, resulting with an original fresh sound.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
