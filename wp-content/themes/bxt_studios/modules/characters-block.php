<?php include('character_modal.php'); ?>
<section type="featured-list">
    <div class="container">
        <div class="dotted-line"></div>
        <div class="header">
            <h2 class="title">Characters</h2>
            <!--
            <a class="header-link" href="#" title="">
                <span class="header-link-arrow">
                            <i class="icon-Right"></i>
                        </span>
                <span class="header-link-text">See all Characters</span>
            </a>
-->
        </div>
        <?php  if(have_rows('characters')): ?>
        <div class="featured-list__wrapper">
             <?php   
                while (have_rows('characters')): the_row(); 
                $character_cover_image = get_sub_field('character_cover_image');
                $character_image = get_sub_field('character_image');                                 
                $character_designation = get_sub_field('character_designation');                                 
                $character_name = get_sub_field('character_name');                                 
                $character_caption = get_sub_field('character_caption');                                 
                $character_details = get_sub_field('character_details');                                 
            ?>
            <a class="featured-list" href="#" data-toggle="modal" data-target="#characterModal1">
                <div class="bkgd fill-screen-bkgd" style="background-image: url('<?php echo $character_cover_image ; ?>"></div>
                <div class="overlay"></div>
                <div class="content">
                    <div class="name"><?php echo $character_name ; ?></div>
                    <div class="post"><?php echo $character_designation ; ?></div>
                    <i class="arrow icon-Right"></i>
                </div>
            </a>
        <?php endwhile; ?>
        </div>
    <?php endif; ?>
    </div>
</section>