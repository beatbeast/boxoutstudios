<head>
    <meta charset="UTF-8">
    <title><?php bloginfo('title'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/img/favicon.png">
    <?php wp_head(); ?>
</head>
