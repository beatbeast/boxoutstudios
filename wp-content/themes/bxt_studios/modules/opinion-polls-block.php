<section>
    <div class="container">
        <div class="dotted__line-wrapper">
            <div class="row mb8">
                <div class="col-md-3">
                    <div class="opinion-teaser-collection">
                        <div class="opinion-teaser opinion-teaser--hero opinion-teaser--stretched">
                            <div class="opinion-teaser__content">
                                <div class="opinion-teaser__meta">
                                    <a href="#" class="opinion-teaser__tag">Opinions</a>
                                </div>
                                <div class="opinion-teaser__heading">
                                    <a href="#">The data revolution can revive the planned economy</a>
                                </div>
                                <p class="opinion-teaser__body">Some, notably in Nigeria, think oceans of information will refloat centralised systems</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="opinion-teaser-collection">
                        <div class="opinion-teaser opinion-teaser--hero opinion-teaser--stretched">
                            <div class="opinion-teaser__content red">
                                <div class="opinion-teaser__meta">
                                    <a href="#" class="opinion-teaser__tag">Opinions</a>
                                </div>
                                <div class="opinion-teaser__heading">
                                    <a href="#">The ethical investment boom</a>
                                </div>
                                <p class="opinion-teaser__body">Will investor moves into funds that uphold ‘environmental, social and governance’ standards really pay off?</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="opinion-teaser-collection">
                        <div class="opinion-teaser opinion-teaser--hero opinion-teaser--stretched">
                            <div class="opinion-teaser__content green">
                                <div class="opinion-teaser__meta">
                                    <a href="#" class="opinion-teaser__tag">Opinions</a>
                                </div>
                                <div class="opinion-teaser__heading">
                                    <a href="#">Profit, not culture, will be Uber’s biggest hurdle</a>
                                </div>
                                <p class="opinion-teaser__body">Without investor subsidies ride-sharing may not be good business</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="opinion-teaser-collection">
                        <div class="opinion-teaser opinion-teaser--hero opinion-teaser--stretched">
                            <div class="opinion-teaser__content">
                                <div class="opinion-teaser__meta">
                                    <a href="#" class="opinion-teaser__tag">Opinions</a>
                                </div>
                                <div class="opinion-teaser__heading">
                                    <a href="#">Plan for five careers in a lifetime</a>
                                </div>
                                <p class="opinion-teaser__body">Work is impermanent — reinvention is rational</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
