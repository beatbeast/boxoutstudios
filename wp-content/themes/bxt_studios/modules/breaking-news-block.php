<section class="pt136">
    <div class="container">
        <div class="latest__news-bar">
            <div class="news-bar-title"> <span>Latest News</span> </div>
            <div class="news-bar-marquee">
                <div class="news-bar-collection" style="animation: news-bar-marquee 30.02s linear infinite;">
                    <div class="news-bar-item">
                        <div class="news-bar-story">
                            <a class="news-bar-item__link" href="#">
                                <span class="news-bar-item__headline">Military Kills Top Boko Haram Commanders</span>
                                <time class="news-bar-item__published-at" datetime="2017-08-31T16:45:16.398Z"> 30 minutes ago </time>
                            </a>
                        </div>
                    </div>
                    <div class="news-bar-item">
                        <div class="news-bar-story">
                            <a class="news-bar-item__link" href="#">
                                <span class="news-bar-item__headline">Soldiers Invade Bayelsa Community Following Ambush By Militants</span>
                                <time class="news-bar-item__published-at" datetime="2017-08-31T16:45:16.398Z"> 34 minutes ago </time>
                            </a>
                        </div>
                    </div>
                    <div class="news-bar-item">
                        <div class="news-bar-story">
                            <a class="news-bar-item__link" href="#">
                                <span class="news-bar-item__headline">Russia Detains Two Over Suspected IS ‘Terror’ Plot</span>
                                <time class="news-bar-item__published-at" datetime="2017-08-31T16:45:16.398Z"> 34 minutes ago </time>
                            </a>
                        </div>
                    </div>
                    <div class="news-bar-item">
                        <div class="news-bar-story">
                            <a class="news-bar-item__link" href="#">
                                <span class="news-bar-item__headline">Pope ‘Deeply Moved’ By Plight Of Storm Harvey Victims</span>
                                <time class="news-bar-item__published-at" datetime="2017-08-31T16:45:16.398Z"> 36 minutes ago </time>
                            </a>
                        </div>
                    </div>
                    <div class="news-bar-item">
                        <div class="news-bar-story">
                            <a class="news-bar-item__link" href="#">
                                <span class="news-bar-item__headline">Brazil Unemployment Drops Unexpectedly To 12.8%</span>
                                <time class="news-bar-item__published-at" datetime="2017-08-31T16:45:16.398Z"> 36 minutes ago </time>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
