 <?php   
    //while (have_rows('creator_profile')): the_row();
    if(have_rows('creator_profile')):
    while (have_rows('creator_profile')): the_row();
    $creator_name = get_sub_field('creator_name');
    $creator_image = get_sub_field('creator_image');                            
    $creator_position = get_sub_field('creator_position');                            
    $creator_details_caption_ = get_sub_field('creator_details_caption_');                            
    $creator_details = get_sub_field('creator_details');                            
?>
<!-- Modal -->
<div class="modal fade" id="founderModal1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card__title">
                    <span class="employee-name"><?=$creator_name ;?></span>
                    <span class="employee-function"><?=$creator_position ;?></span>
                    <ul class="employee-social-links">
                        <li>
                            <a href="https://twitter.com/#" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/#" target="_blank">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/#" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <hr>
                <p class="caption-text"><?=$creator_details_caption_ ;?></p>
                <p><?=$creator_details ;?></p>
            </div>
        </div>

    </div>
</div>
<?php endwhile; endif; ?>