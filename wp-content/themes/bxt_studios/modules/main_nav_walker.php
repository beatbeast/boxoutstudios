<?php

/**
 *
 */
class bxt_nav_walker extends Walker_Nav_menu
{

  function start_lvl ( &$output, $depth = 0, $args = array() ) {
    $indent = ($depth)? str_repeat("\t", $depth) : '';
    $submenu = ($depth > 0)? '' : 'dropdown ddt2';
    $output .=  "\n$indent<ul class=\"$submenu depth_$depth\">\n";
  }

  function start_el ( &$output, $item, $depth = 0, $args = array(),  $id = 0) {
    $indent = ($depth)? str_repeat("\t", $depth) : '';
		$output .= $indent . '<li>';
    $link_attributes= '';
    $item_output = '';

		$link_attributes .= ! empty($item->attr_title ) ? 'title="' . esc_attr($item->attr_title) . '"' : '';
		$link_attributes .= ! empty($item->target ) ? 'target="' . esc_attr($item->target) . '"' : '';
		$link_attributes .= ! empty($item->xfn ) ? 'rel="' . esc_attr($item->xfn) . '"' : '';
		if( ! empty($item->url ) ){
			if($args->walker->has_children ){
				$link_attributes .= 'href="' . esc_attr($item->url) . '"';
			}else{
			$link_attributes .= 'href="' . esc_attr($item->url) . '"' ;
			}

		}else {
			$link_attributes .= 'href=" "';
		}
		$item_output .= $item->before;
		$item_output .= '<a ' . $link_attributes . '>';
		$item_output .= $item->link_before . apply_filters('the_title', $item->title, $item->ID ) . $item->link_after;
		$item_output .= '</a>';
	  $item_output .= $item->after;
	  $output .= apply_filters ('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
  }

  function end_lvl( &$output, $depth = 0, $args = array() ){		//close ul
    $indent = ($depth)? str_repeat("\t", $depth) : '';
    $before_ul_close = ($depth !== 0)? "\n".' ' : '';
    $output .= $before_ul_close."\n".'</ul>';
	}
}


 ?>
