<section type="page-links">
    <div class="container">
        <article class="item">
            <a href="#" class="link">
                <span class="suplabel">About Bxt Studio</span>
                <span class="item-title">The WOW factor</span>
                <i class="icon-Right"></i>
            </a>
        </article>

        <article class="item">
            <a href="#" class="link">
                <span class="suplabel">Press</span>
                <span class="item-title">See us in the News</span>
                <i class="icon-Right"></i>
            </a>
        </article>

        <article class="item">
            <a href="#" class="link">
                <span class="suplabel">Our Merchandise</span>
                <span class="item-title">Change your style</span>
                <i class="icon-Right"></i>
            </a>
        </article>
    </div>
</section>
