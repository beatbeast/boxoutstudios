<?php
	//Get Field Variables
	$comic_page_banner_image = get_field('comic_page_banner_image');

	 if ($comic_page_banner_image == ''):
?>


	<section class="header_wrap bg-image" style="background-image: url('assets/img/comic-cover-33.jpg');"></section>



 <?php else: ?>

	<section class="header_wrap bg-image" style="background-image: url('<?php echo $comic_page_banner_image; ?>');"></section>


 <?php endif; ?>  