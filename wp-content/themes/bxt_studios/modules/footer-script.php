<?php wp_footer(); ?>
<!-- Script -->
<script src="<?php bloginfo('template_directory'); ?>/js/vendor/jquery-1.12.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/flexslider.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/menuzord.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/parallax.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.nice-select.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/aos.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.counterup.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/waypoints.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/lightbox.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/venobox/venobox.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.easing.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/camera.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.parallax-1.1.3.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<!-- <script src="<?//php bloginfo('template_directory'); ?>/js/custom.js"></script> -->
<script>
    if ($('.slider-all-controls, .slider-paging-controls, .slider-arrow-controls, .slider-thumb-controls, .logo-carousel').length) {
        $('.slider-all-controls').flexslider({
            start: function(slider) {
                if (slider.find('.slides li:first-child').find('.fs-vid-background video').length) {
                    slider.find('.slides li:first-child').find('.fs-vid-background video').get(0).play();
                }
            },
            after: function(slider) {
                if (slider.find('.fs-vid-background video').length) {
                    if (slider.find('li:not(.flex-active-slide)').find('.fs-vid-background video').length) {
                        slider.find('li:not(.flex-active-slide)').find('.fs-vid-background video').get(0).pause();
                    }
                    if (slider.find('.flex-active-slide').find('.fs-vid-background video').length) {
                        slider.find('.flex-active-slide').find('.fs-vid-background video').get(0).play();
                    }
                }
            }
        });
        $('.slider-paging-controls').flexslider({
            animation: "slide",
            directionNav: false
        });
        $('.slider-arrow-controls').flexslider({
            controlNav: false
        });
        $('.slider-thumb-controls .slides li').each(function() {
            var imgSrc = $(this).find('img').attr('src');
            $(this).attr('data-thumb', imgSrc);
        });
        $('.slider-thumb-controls').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: true
        });
        $('.logo-carousel').flexslider({
            minItems: 1,
            maxItems: 4,
            move: 1,
            itemWidth: 200,
            itemMargin: 0,
            animation: "slide",
            slideshow: true,
            slideshowSpeed: 3000,
            directionNav: false,
            controlNav: false
        });
    }

    $(document).ready(function() {
        //carousel options
        $('#quote-carousel').carousel({
            pause: true,
            interval: 6000,
        });
    });

</script>
