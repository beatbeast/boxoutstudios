<section>
    <div class="nvaigation_area side_padding type3 dark drop_down1 dropDowncmn mega_menu1 full_width">
        <div id="menuzord3" class="menuzord dark menuzord-responsive">
            <a href="<?php echo site_url(); ?>" class="menuzord-brand">
                <img src="<?php bloginfo('template_directory'); ?>/img/logo-squares-small.png" class="image-lg" alt="BXT Studio logo Light">
            </a>
            <?php
            // $walker = new my_nav_walker;

            // wp_nav_menu( array( 
            //     'theme_location' =>'main-menu',
            //     'menu_class' => 'menuzord-menu', 
            //     'walker' => '$walker',
            // ) );

                wp_nav_menu( array(
                    'theme_location'    => 'main-menu',
                    'depth'             => 5,
                    'container'         => '',
                    'container_class'   => '',
                    'container_id'      => '',
                    'menu_class'        => 'menuzord-menu',
                    'fallback_cb'       => '',
                    'walker' => new bxt_nav_walker(),
                ) );
            ?>
            <!-- <ul class="menuzord-menu"> -->
                <!-- <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li>
                    <a href="javascript:void(0)">Comics</a>
                    <div class="megamenu megamenu-half-width megamenu3 parent_content ">
                        <div class="megamenu-row eqh ">
                            <div class="col12">
                                <div class="mega_menu_half_content">
                                    <div class="main-menu-comic">
                                        <a href="comic.php" class="comic-poster">
                                            <img src="<?php //bloginfo('template_directory'); ?>/img/menu-comic-img-33.jpg" alt="">
                                        </a>
                                        <span class="comic-release-date">September 22, 2017</span>
                                        <a href="comic.php" class="comic-title">Dream Catchers: The Golden Circle</a>
                                    </div>

                                    <div class="main-menu-comic">
                                        <a href="comic.php" class="comic-poster">
                                            <img src="<?php //bloginfo('template_directory'); ?>/img/menu-comic-img-shepherd.jpg" alt="">
                                        </a>
                                        <span class="comic-release-date">September 22, 2017</span>
                                        <a href="comic.php" class="comic-title">Blacksage: The Rising</a>
                                    </div>

                                    <div class="main-menu-comic">
                                        <a href="comic.php" class="comic-poster">
                                            <img src="<?php //bloginfo('template_directory'); ?>/img/menu-comic-img-blacksage.jpg" alt="">
                                        </a>
                                        <span class="comic-release-date">September 22, 2017</span>
                                        <a href="comic.php" class="comic-title">33: Muquanna Recovery</a>
                                    </div>

                                    <div class="main-menu-comic">
                                        <a href="comic.php" class="comic-poster">
                                            <img src="<?php //bloginfo('template_directory'); ?>/img/menu-comic-img-blacksage.jpg" alt="">
                                        </a>
                                        <span class="comic-release-date">September 22, 2017</span>
                                        <a href="comic.php" class="comic-title">Shepherd: The Rise of a King</a>
                                    </div>

                                    <div class="main-menu-comic">
                                        <a href="comic.php" class="comic-poster">
                                            <img src="<?php //bloginfo('template_directory'); ?>/img/menu-comic-img-blacksage.jpg" alt="">
                                        </a>
                                        <span class="comic-release-date">September 22, 2017</span>
                                        <a href="comic.php" class="comic-title">Shepherd: The Rise of a King</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="javascript:void(0)">Gallery</a>
                    <ul class="dropdown ddt2">
                        <li><a href="gallery-characters.php">Characters</a></li>
                        <li><a href="#">Fan Art/Submissions</a></li>
                        <li><a href="#">Photos</a></li>
                    </ul>
                </li>
                <li><a href="press.php">Press</a></li>
                <li><a href="#">Shop</a></li>
                <li><a href="support.php">Support</a></li>
                <li><a href="contact.php">Contact</a></li> -->
            <!-- </ul> -->
        </div>
    </div>
</section>
