<section type="contact">
    <div class="container">
        <div class="container-inner">
            <h2 class="section-title">Contact us</h2>
            <figure class="contact-img">
                <img src="<?php bloginfo('template_directory'); ?>/img/contact-img.jpg" alt="">
            </figure>
            <p class="contact-text">For more information or questions, please contact Sola Adebayo <br>at <a href="mailto:s.a@bxtstudios.com" title="">s.a@bxtstudios.com</a> or call +234 812 703 6493.</p>
        </div>
    </div>
</section>
