<?php  if(have_rows('banner_slider')):  /* parent repeater */ ?>
        <section class="banner_area4 clearfix">
            <div class="camera_wrap " id="banner_slider2">
            <?php   
             while (have_rows('banner_slider')): the_row(); /* parent repeater */
                 $banner_image = get_sub_field('banner_image');
                 $banner_header = get_sub_field('banner_header');
                 $banner_caption = get_sub_field('banner_caption');
                 $cta_label = get_sub_field('cta_label');
                 $cta_link = get_sub_field('cta_link'); ?>
                 <div class="hero_slider" data-src="<?=$banner_image ; ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-8  col-sm-10 col_exception">
                                <div class="slider_inner_area">
                                    <div class="slider_inner_content align-left">
                                        <h1 class="fadeInUp banner_title" style="animation-duration: 1s; -webkit-animation-duration: 1s; animation-fill-mode: both; animation-delay: .0s;-webkit-animation-delay: .0s;">
                                            <?=$banner_header ; ?>
                                        </h1>
                                        <p class="fadeInUp" style="animation-duration: 1s; -webkit-animation-duration: 1s; animation-fill-mode: both; animation-delay: .0s;-webkit-animation-delay: .0s;">
                                            <?=$banner_caption ; ?>
                                        </p>
                                        <a class="intro-link" href="<?=$cta_link ; ?>" style="animation-duration: 1s; -webkit-animation-duration: 1s; animation-fill-mode: both; animation-delay: .0s;-webkit-animation-delay: .0s;">
                                            <svg class="icon icon-generic" width="30" height="30" viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" style="fill-rule:evenodd;clip-rule:evenodd;">
                                                    <g transform="matrix(1,0,0,1,1.56155,1.56155)">
                                                        <circle cx="13" cy="13" r="12.5" style="fill:none;stroke:rgb(255,255,255);stroke-width:1px;"></circle>
                                                        <path d="M16.5,13L10.5,9.5L10.5,16.5L16.5,13" style="fill:rgb(255,255,255);fill-rule:nonzero;"></path>
                                                    </g>
                                                </svg> <?=$cta_label ; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
        </section>
<?php endif; ?>