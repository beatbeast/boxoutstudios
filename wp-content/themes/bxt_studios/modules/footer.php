<?php 
    $contact_email = get_field('contact_email','option');
    $phone_number = get_field('contact_telephone_number','option');
?>
    <!--
    <footer class="pb0">
        <div class="footer__wrapper">
            <div class="container">
                <div class="row">
                    <div class="footer__newsletter">
                        <div class="newsletter">
                            <div class="newsletter__callout">
                                <h2>Subscribe for updates</h2>
                                <p class="newsletter__callout__description">Get access to news update, downloads and freebies.</p>
                            </div>
                            <form action="#" method="post" target="_blank" class="footer__form">
                                <div class="newsletter__form__field">
                                    <input type="email" name="EMAIL" placeholder="Email" class="footer__form__input">
                                    <input type="submit" value="Subscribe" name="subscribe" class="footer__form__submit">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="footer__socialmedia">
                        <div class="footer__socialmedia-title">Follow Us</div>
                        <div class="footer__socialmedia-group">
                            <a href="https://www.facebook.com/#" class="social-share--facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/#" class="social-share--twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.youtube.com/#" class="social-share--youtube" target="_blank"><i class="fa fa-youtube-play"></i></a>
                            <a href="https://www.instagram.com/#" class="social-share--instagram" target="_blank"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="footer__footnote mb-xs-24">
                <aside>
                    <p>© 2017 Boxtout Studio. All rights reserved.</p>
                </aside>
                <div class="footer__links">
                    <ul>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Terms</a></li>
                        <li><a href="#">Company</a></li>
                        <li><a href="#">Press</a></li>
                        <li><a href="#">Careers</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center hidden-xs">
                    <p class="micro">"Bxt Studio" is a registered trademark of Boxtout Studio.</p>
                </div>
            </div>
        </div>
    </footer>
    -->

    <footer>
        <div class="letstalk">
            <div>
                <a href="#">
                    <h2>Let's Talk</h2>
                    <p>
                        Are you a writer or an artist?<br> Would you like to join our creator-owned family?
                    </p>
                    <strong>Pull the Trigger</strong>
                </a>
            </div>
        </div>
        <div class="social">
            <div class="scroller">
                <div class="btns">
                    <ul>
                        <li>
                            <div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/weareshellshock" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=261246470578510&amp;container_width=51&amp;href=https%3A%2F%2Fwww.facebook.com%2Fweareshellshock&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false"><span style="vertical-align: bottom; width: 73px; height: 20px;"><iframe name="f20e6f192d9e8f4" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.4/plugins/like.php?action=like&amp;app_id=261246470578510&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FlY4eZXm_YWu.js%3Fversion%3D42%23cb%3Df14e98eaa0cc028%26domain%3Dweareshellshock.com%26origin%3Dhttps%253A%252F%252Fweareshellshock.com%252Ff3ef7ab3f2b9bb4%26relation%3Dparent.parent&amp;container_width=51&amp;href=https%3A%2F%2Fwww.facebook.com%2Fweareshellshock&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=false" style="border: none; visibility: visible; width: 73px; height: 20px;" class=""></iframe></span></div>
                        </li>
                        <li class="tw">
                            <iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-follow-button twitter-follow-button-rendered" title="Twitter Follow Button" src="https://platform.twitter.com/widgets/follow_button.64457a6e8f493e1f1022f50e4e7e67e8.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;screen_name=weareshellshock&amp;show_count=false&amp;show_screen_name=false&amp;size=m&amp;time=1513430138566" style="position: static; visibility: visible; width: 65px; height: 20px;" data-screen-name="weareshellshock"></iframe>
                            <script>
                                ! function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0],
                                        p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id)) {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + '://platform.twitter.com/widgets.js';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }(document, 'script', 'twitter-wjs');

                            </script>
                        </li>
                    </ul>
                </div>
                <div class="c2a">
                    <span>Like Us</span>
                </div>
            </div>
        </div>
        <div class="info">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <div itemscope="" itemtype="http://schema.org/ProfessionalService">
                                <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                    <a href="mailto:hello@bxtstudios.com" target="_blank">
                                        <span><?=$contact_email ; ?></span>
                                    </a><br>
                                </div>
                                <div>
                                    <span itemprop="telephone"><a href="tel:+234<?=$phone_number ; ?>">+234 <?=$phone_number ; ?></a></span>
                                </div>
                            </div>
                            <span class="copyright">COPYRIGHT <?php echo date("Y"); ?> BY BXT STUDIOS INC.</span>
                        </td>
                        <td class="network">
                            <div>
                                <p>
                                    <a href="https://www.facebook.com/#" target="_blank">Facebook</a>
                                    <a href="https://twitter.com/#" target="_blank">Twitter</a>
                                    <a href="https://instagram.com/#" target="_blank">Instagram</a>
                                </p>
                                <p>
                                    <a href="#">Creators</a>
                                    <a href="#">Blog</a>
                                    <a href="#">Terms</a>
                                </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </footer>

<a href="#" class="go-top top" style="display: block;">
    <div class="go-top-arrow go-top-arrow-top"></div>
    <div class="go-top-label-cont">
        <span class="go-top-label go-top-label-top">Back to top</span>
    </div>
</a>
