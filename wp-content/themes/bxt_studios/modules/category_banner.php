<?php
    //Define ACF Page banner variables
    $terms = get_the_terms( $post, 'category');
    $term = $terms[0];
    
    $category_banner_image = get_field('category_banner_image', $term);
    $category_banner_header = get_field('category_banner_heading', $term);
     if ($category_banner_image == ''): ?>
        <section class="breadCrumb_area breadCrumb2 support" style='background: url(<?php echo get_bloginfo('template_directory').'/img/badge.png' ; ?>);background-repeat:no-repeat; background-size: cover;'>
            <div class="overlay2"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bread_crumb_inner_area alignment_row">
                            <h2 class="bh__title"><?=$category_banner_header?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php else: ?>

        <section class="breadCrumb_area breadCrumb2 support" style='background: url(<?=$category_banner_image?>); background-repeat:no-repeat; background-size: cover;'>
            <div class="overlay2"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bread_crumb_inner_area alignment_row">
                            <h2 class="bh__title"><?=$category_banner_header?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>