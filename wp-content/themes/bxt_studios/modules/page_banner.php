<?php
    //Define ACF Page banner variables
    
    $page_banner_image = get_field('banner_image', $term);
    $page_banner_header = get_field('banner_header', $term);
     if ($page_banner_image == ''): ?>
        <section class="breadCrumb_area breadCrumb2 support" style='background: url(<?php echo get_bloginfo('template_directory').'/img/badge.png' ; ?>);background-repeat:no-repeat; background-size: cover;'>
            <div class="overlay2"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bread_crumb_inner_area alignment_row">
                            <h2 class="bh__title"><?=$page_banner_header ; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    <?php else: ?>

        <section class="breadCrumb_area breadCrumb2 support" style='background: url(<?=$page_banner_image?>); background-repeat:no-repeat; background-size: cover;'>
            <div class="overlay2"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bread_crumb_inner_area alignment_row">
                            <h2 class="bh__title"><?=$page_banner_header ; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>


<?php endif; ?>        