<section class="videos">
    <div class="container">
        <div class="row">
            <div class="section__title">
                <a href="#" class="section__title-link">Videos</a>
            </div>
            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Bilateral Ties: British Foreign Secretary Visits Nigeria">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img1.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Bilateral Ties: British Foreign Secretary Visits Nigeria</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Army, Air Force Conduct Special Operation In Borno">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img2.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Army, Air Force Conduct Special Operation In Borno</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Ships Emitting Polluted Substances Cannot Dock In Nigeria - Dakuku Peterside">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img3.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Ships Emitting Polluted Substances Cannot Dock In Nigeria - Dakuku Peterside</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Odumakin Stresses The Need For Local Government Autonomy">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img4.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Odumakin Stresses The Need For Local Government Autonomy</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="The Local Government System Needs Restructuring - West Idahosa">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img5.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>The Local Government System Needs Restructuring - West Idahosa</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Johnson Pays Tribute To Nigeria's Fallen Heroes">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img6.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Johnson Pays Tribute To Nigeria's Fallen Heroes</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Operation 'Lafiya Dole' Discloses Latest Figures On Activities">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img7.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Operation 'Lafiya Dole' Discloses Latest Figures On Activities</span></p>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="video-holder">
                    <a href="#" style="">
                        <div class="image_holder" alt="Alleged Kidnapping: LASG Arraigns 'Evans' And 5 Others">
                            <img class="image" src="<?php bloginfo('template_directory'); ?>/img/video-img8.jpg" alt="">
                            <div class="video-control video-control--play"></div>
                        </div>
                        <p><span>Alleged Kidnapping: LASG Arraigns 'Evans' And 5 Others</span></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
