<!-- /* Template Name: Support Page */ -->
<?php get_header(); 
     /* Get the field variable */ 
  $page_title = get_field('page_title');
  $page_caption = get_field('page_caption');
  $sidebar_image = get_field('sidebar_image');
?>

       <!--  <section class="breadCrumb_area breadCrumb2 support">
            <div class="overlay2"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bread_crumb_inner_area alignment_row">
                            <h2 class="bh__title">Support</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->


        <section class="pt48 pb96" type="support">
            <div class="container">
                <div class="row mb24">
                    <div class="col-md-8">
                        <h2><?=$page_title; ?></h2>
                        <p><?=$page_caption; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 mb32">
                       <!--  <form>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="nice-select input_field2 mb32">
                                        <div class="fields">
                                            <span class="current">Thirty Three - 33</span>
                                            <ul class="list">
                                                <li data-value="1" class="option selected focus">Thirty Three -33</li>
                                                <li data-value="2" class="option">Black Sage</li>
                                                <li data-value="3" class="option">Dream Catchers</li>
                                                <li data-value="4" class="option">The Shepherd</li>
                                            </ul>
                                            <label for="Field7">Select Comic you'll like to support</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="dotted mb40">

                            <div class="row">
                                <div class="col-md-6 col-md-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <label for="first-name">First Name <span class="req">*</span></label>
                                            <input type="text" name="fname" placeholder="" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <label for="last-name">Last Name <span class="req">*</span></label>
                                            <input type="text" name="lname" placeholder="" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <label for="email">Email <span class="req">*</span></label>
                                            <input type="text" name="email" placeholder="" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <label for="phone">Phone</label>
                                            <input type="text" name="phone" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <label for="addr">Address <span class="req">*</span></label>
                                            <input type="text" name="addr" placeholder="" required="">
                                            <label for="Field3">Street Address</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <input type="text" name="city" placeholder="" required="">
                                            <label for="Field4">City</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <input type="text" name="state" placeholder="" required="">
                                            <label for="Field5">State / Province / Region</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <input type="text" name="ip" placeholder="" required="">
                                            <label for="Field6">Postal / Zip Code</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="nice-select input_field2">
                                        <div class="fields">
                                            <span class="current">Nigeria</span>
                                            <ul class="list">
                                                <li data-value="1" class="option selected focus">Nigeria</li>
                                                <li data-value="2" class="option">Ghana</li>
                                                <li data-value="3" class="option">Spain</li>
                                                <li data-value="4" class="option">United Kingdom</li>
                                            </ul>
                                            <label for="Field7">Country</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <h3>Pledge Amount</h3>
                                    <p>When you become an Annual Fund member, you support our work to bring dynamic science education to nearly 1.5 million guests of all ages AND you receive out-of-this-world benefits, including free parking and admission for you, your family and two guests, free passes to special exhibitions, tours and the Giant Dome Theater, and invitations to MSInsider events. Your gift to the Annual Fund is 100% tax deductible.</p>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="input_field2">
                                        <div class="fields">
                                            <label class="choice">$10
                                                <input type="radio" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="choice">$25
                                                <input type="radio" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="choice">$50
                                                <input type="radio" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="choice">$100
                                                <input type="radio" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="choice">I'll like to specify the amount
                                                <input type="radio" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <a class="funky-btn" href="#">
                                        <div class="text">Submit</div>
                                        <div class="bg"></div>
                                    </a>
                                </div>
                            </div>
                        </form> -->
                        <?php echo do_shortcode( '[contact-form-7 id="186" title="Support Form"]' ); ?>
                    </div>

                    <div class="col-md-4 col-md-offset-1 col-sm-12">
                        <img src="<?=$sidebar_image; ?>" class="pledge-image" alt="" />
                        <ul>
                            <li class="pledge-sidebar">
                                <div class="pledge__info">
                                    <h4 class="pledge__amount">Pledge <span class="money">$10</span> or more</h4>
                                    <div class="pledge__reward-description">
                                        <p>Pledge the above amount and get the following items.</p>
                                        <ul>
                                            <li class="list-disc">Thank you email from the creator</li>
                                            <li class="list-disc">2 Digital Issues of 33's 15 page web comic</li>
                                            <li class="list-disc">1 Drawing process video</li>
                                            <li class="list-disc">1 Digital Artwork</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>

                            <li class="pledge-sidebar">
                                <div class="pledge__info">
                                    <h4 class="pledge__amount">Pledge <span class="money">$25</span> or more</h4>
                                    <div class="pledge__reward-description">
<!--                                        <p>Pledge the above amount and get the following items.</p>-->
                                        <ul>
                                            <li class="list-disc">2 Digital Issues of 33's 15 page web comic</li>
                                            <li class="list-disc">*1 Signed physical copy of Graphic Novel</li>
                                            <li class="list-disc">2 Drawing process video</li>
                                            <li class="list-disc">2 Digital Artwork</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>

                            <li class="pledge-sidebar">
                                <div class="pledge__info">
                                    <h4 class="pledge__amount">Pledge <span class="money">$50</span> or more</h4>
                                    <div class="pledge__reward-description">
<!--                                        <p>Pledge the above amount and get the following items.</p>-->
                                        <ul>
                                            <li class="list-disc">5 Digital Issues of 33's 15 page web comic</li>
                                            <li class="list-disc">*2 Signed physical copies of Graphic Novel</li>
                                            <li class="list-disc">2 Drawing process video</li>
                                            <li class="list-disc">4 Digital Artwork</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>

                            <li class="pledge-sidebar">
                                <div class="pledge__info">
                                    <h4 class="pledge__amount">Pledge <span class="money">$100</span> or more</h4>
                                    <div class="pledge__reward-description">
<!--                                        <p>Pledge the above amount and get the following items.</p>-->
                                        <ul>
                                            <li class="list-disc">5 Digital Issues of 33's 15 page web comic</li>
                                            <li class="list-disc">*4 Signed physical copies of Graphic Novel</li>
                                            <li class="list-disc">1 T-shirt and 1 snap back</li>
                                            <li class="list-disc">5 Digital Artwork</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); ?>